﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestingChallenge;

namespace Tests
{
    /// <summary>
    /// Our NUnit test methods are public methods decorated with a [Test] attribute.
    /// </summary>
    public class CallerTests
    {
       [Test]
       public void ExampleTestMethod()
        {
            //Arrange

            //Act

            //Assert (NUnit Assertions are made called via the Assert class' methods.)

        }
    }
}
