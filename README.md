Our business needs to automatically filter a phone call from a Person, depending on some criteria.
We have created a PhoneCallRouter type with a single method - WillAcceptCall(). This method takes a Person object as a parameter, and will accept the call (return true) or decline (return false) depending on our criteria.

The WillAcceptCall() needs to fulfill the requirements -

Requirements: 
1. We should reject calls from "Steven Legoface", case insensitively.
2. We can only accept valid phone numbers that are exactly 11 digits long - "x (xxx) xxx xxxx".
3. We must reject all calls from Steven Legoface, case insensitively
4. We must reject all calls from senders with names beginning with "Garrett", case insensitively
5. Callers must be 18 or older to be accepted.

Given these requirements, the WillAcceptCall() method currently has several bugs.

Your challenge is to:
1. Fix the logic in WillAcceptCall() so that the above requirements are met exactly as described, and- 
2. Write Unit tests that test the WillAcceptCall() so that our requirements are covered.

The provided solution contains two projects: 
"TestingChallenge", containing our PhoneCallRouter and Person types. 
"Tests" containing a single CallerTests class with one unit test method example you can reference.