﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestingChallenge
{
    public class PhoneCallRouter
    {
        public bool WillAcceptCall(Person personThatCalled)
        {
            switch (personThatCalled)
            {
                case Person p when p.Name == "Steven Legoface":
                    return false;

                case Person p when p.PhoneNumber.Length >= 11:
                    return false;

                case Person p when p.Name == "Garrett":
                    return false;

                case Person p when p.Age <= 18:
                    return false;

                default: return true;
            }
        }
    }
    public class Person
    {
        public Person (string name, string phoneNumber, int age)
        {
            Name = name;
            PhoneNumber = phoneNumber;
            Age = age;
        }

        public int Age { get; }
        public string Name { get; }
        public string PhoneNumber { get; }
    }
    
}